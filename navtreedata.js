/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Contents:", "index.html#sec_pages", null ],
    [ "Lab 1 Motor Driver", "lab1.html", null ],
    [ "Lab 2 Encoder Driver", "lab2.html", null ],
    [ "Lab 3 Tuning Feedback Motor System", "page3.html", null ],
    [ "Lab 4 IMU Driver", "lab4.html", null ],
    [ "Project Proposal: Candle Extinguisher", "page4.html", null ],
    [ "Final Project: Candle Extinguisher", "finalproject.html", [
      [ "Mechatronics Automatic Candle Extinguisher", "finalproject.html#ME405", [
        [ "Video", "lab4.html#Video", null ],
        [ "Problem Statement", "page4.html#subsec_problem", null ],
        [ "Project Requirements", "page4.html#subsec_project", null ],
        [ "Materials", "page4.html#Materials", null ],
        [ "Bill of Materials", "page4.html#subsec_bom", null ],
        [ "Assembly Plan", "page4.html#subsec_assemblyplan", [
          [ "Bitbucket", "lab1.html#Bitbucket", null ],
          [ "Purpose", "lab1.html#Purpose", null ],
          [ "Usage", "lab1.html#Usage", null ],
          [ "Testing", "lab1.html#Testing", null ],
          [ "Bugs and Limitations", "lab1.html#subsubsec_bugs", null ],
          [ "Bugs and Limitations", "lab2.html#subsec_bugs", null ],
          [ "Tuning", "page3.html#Tuning", null ],
          [ "Sub assembly 1 (base components)", "page4.html#subsubsec_assem1", null ],
          [ "Sub assembly 2 (arm mechanism)", "page4.html#subsubsec_assem2", null ],
          [ "Sub assembly 3 (electrical housing)", "page4.html#subsub_assem3", null ]
        ] ],
        [ "Safety Assessment", "page4.html#subsec_safety", null ],
        [ "Project Timeline and Tasks", "page4.html#subsec_timeline", [
          [ "Week of May 24:", "page4.html#subsubsec_may24", null ],
          [ "Week of May 31:", "page4.html#subsubsec_may31", null ],
          [ "Week of June 8:", "page4.html#subsubsec_june8", null ],
          [ "Testing Bloopers!:", "finalproject.html#subsub_bloop", null ]
        ] ],
        [ "Bitbucket Code", "finalproject.html#subsec_bitbuck", null ],
        [ "Hardware", "finalproject.html#subsec_hardware", null ],
        [ "Software", "finalproject.html#subsec_software", [
          [ "Main", "finalproject.html#subsubsec_main", null ],
          [ "Task: CheckBtn", "finalproject.html#subsubsec_taskbtn", null ],
          [ "Tasks: Timer1, Timer2, and HoldLidTimer", "finalproject.html#subsubsec_tasktimers", null ],
          [ "Task: MoveToCandle", "finalproject.html#subsubsec_taskmovetocandle", null ],
          [ "Task: MoveLid", "finalproject.html#subsub_taskmovelid", null ],
          [ "Task: GoHome", "finalproject.html#subsub_taskgohome", null ]
        ] ],
        [ "Results", "finalproject.html#Results", [
          [ "Limitations and Debugging", "finalproject.html#subsub_limits", null ],
          [ "Future Works", "finalproject.html#subsub_futureworks", null ]
        ] ],
        [ "Acknowledgements", "finalproject.html#Acknowledgements", null ]
      ] ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classpyb_1_1USB__VCP.html#ae478c50edbbac8cf860a55b482b82c8e"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';