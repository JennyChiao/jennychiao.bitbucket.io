var annotated_dup =
[
    [ "encoder", "namespaceencoder.html", "namespaceencoder" ],
    [ "Hall", null, [
      [ "HallEffectSensor", "classHall_1_1HallEffectSensor.html", "classHall_1_1HallEffectSensor" ]
    ] ],
    [ "imudriver", "namespaceimudriver.html", "namespaceimudriver" ],
    [ "keypad", "namespacekeypad.html", "namespacekeypad" ],
    [ "kpcontrol", "namespacekpcontrol.html", "namespacekpcontrol" ],
    [ "main_finalproject", null, [
      [ "CheckBtn", "classmain__finalproject_1_1CheckBtn.html", "classmain__finalproject_1_1CheckBtn" ],
      [ "GoHome", "classmain__finalproject_1_1GoHome.html", "classmain__finalproject_1_1GoHome" ],
      [ "HoldLidTimer", "classmain__finalproject_1_1HoldLidTimer.html", "classmain__finalproject_1_1HoldLidTimer" ],
      [ "MoveLid", "classmain__finalproject_1_1MoveLid.html", "classmain__finalproject_1_1MoveLid" ],
      [ "MoveToCandle", "classmain__finalproject_1_1MoveToCandle.html", "classmain__finalproject_1_1MoveToCandle" ],
      [ "Timer1", "classmain__finalproject_1_1Timer1.html", "classmain__finalproject_1_1Timer1" ],
      [ "Timer2", "classmain__finalproject_1_1Timer2.html", "classmain__finalproject_1_1Timer2" ]
    ] ],
    [ "motordriver", null, [
      [ "MotorDriver", "classmotordriver_1_1MotorDriver.html", "classmotordriver_1_1MotorDriver" ]
    ] ],
    [ "pyb", null, [
      [ "_board", "classpyb_1_1__board.html", "classpyb_1_1__board" ],
      [ "Accel", "classpyb_1_1Accel.html", "classpyb_1_1Accel" ],
      [ "ADC", "classpyb_1_1ADC.html", "classpyb_1_1ADC" ],
      [ "CAN", "classpyb_1_1CAN.html", "classpyb_1_1CAN" ],
      [ "DAC", "classpyb_1_1DAC.html", "classpyb_1_1DAC" ],
      [ "ExtInt", "classpyb_1_1ExtInt.html", "classpyb_1_1ExtInt" ],
      [ "I2C", "classpyb_1_1I2C.html", "classpyb_1_1I2C" ],
      [ "LCD", "classpyb_1_1LCD.html", "classpyb_1_1LCD" ],
      [ "LED", "classpyb_1_1LED.html", "classpyb_1_1LED" ],
      [ "Pin", "classpyb_1_1Pin.html", "classpyb_1_1Pin" ],
      [ "PinAF", "classpyb_1_1PinAF.html", "classpyb_1_1PinAF" ],
      [ "RTC", "classpyb_1_1RTC.html", "classpyb_1_1RTC" ],
      [ "Servo", "classpyb_1_1Servo.html", "classpyb_1_1Servo" ],
      [ "SPI", "classpyb_1_1SPI.html", "classpyb_1_1SPI" ],
      [ "Switch", "classpyb_1_1Switch.html", "classpyb_1_1Switch" ],
      [ "Timer", "classpyb_1_1Timer.html", "classpyb_1_1Timer" ],
      [ "TimerChannel", "classpyb_1_1TimerChannel.html", "classpyb_1_1TimerChannel" ],
      [ "UART", "classpyb_1_1UART.html", "classpyb_1_1UART" ],
      [ "USB_HID", "classpyb_1_1USB__HID.html", "classpyb_1_1USB__HID" ],
      [ "USB_VCP", "classpyb_1_1USB__VCP.html", "classpyb_1_1USB__VCP" ]
    ] ]
];